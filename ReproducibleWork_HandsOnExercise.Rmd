---
title: "Hands on Exercise Reproducible Work"
author: "Xavier"
date: "2024-04-10"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Session Reproducible Work

Wednesday April 10, 2024. IL3-UB.

Related to: <https://seeds4c.org/reproduciblework2023>

## Hands on Exercise

```{r}
if (!require(readr)) {install.packages("readr")}
if (!require(dplyr)) {install.packages("dplyr")}
if (!require(tidyr)) {install.packages("tidyr")}
```

```{r}
if (!file.exists("data_subset_all.csv")) {
  system("wget http://cloud.seeds4c.org/data_smc.csv.bz2")
  system("bunzip2 data_smc.csv.bz2 -k")
  system("cat data_smc.csv | head -n1000001 > data_subset_all.csv")
}
data <- read_csv("data_subset_all.csv")
```

```{r}
# Get the description of the variable codes
# From here: https://analisi.transparenciacatalunya.cat/Medi-Ambient/Metadades-variables-meteorol-giques/4fb2-n3yi/data
variables <- read_csv("https://analisi.transparenciacatalunya.cat/api/views/4fb2-n3yi/rows.csv?accessType=DOWNLOAD&sorting=true")
```

```{r}
# We prepare a small dataframe from the variable definition to join on the smc data frame
variables.to.join <- variables %>% 
  select(CODI_VARIABLE, ACRONIM) %>% 
  arrange(CODI_VARIABLE)

variables.to.join
```

```{r}
# Let's join variable df on to the data df
data <- left_join(data, variables.to.join) %>% 
  rename(ACRONIM_VARIABLE = ACRONIM)
```

```{r}
# Let's convert the source data frame (which is long shapè, as database) into a wide shape (table like, with meteorological variables as columns) while selecting just one meteorological station as an example
data_wide <- data %>% 
  filter(CODI_ESTACIO =="D5") %>% # D5 corresponds to "Barcelona Observatori Fabra" Meteorological Observatory (at Collserola Mountain) https://analisi.transparenciacatalunya.cat/Medi-Ambient/Metadades-estacions-meteorol-giques-autom-tiques/yqwd-vj5e
  select(
    ACRONIM_VARIABLE, 
    DATA_LECTURA,
    VALOR_LECTURA) %>% 
  pivot_wider(
    names_from = "ACRONIM_VARIABLE",
    values_from = "VALOR_LECTURA")

data_wide
```

```{r}
# Save resulting dataset to disk
write_csv(data_wide, "data_subset_d5_wide.csv")
```


```{r}
# Produce a simple R version of this R Markdown document
knitr::purl("ReproducibleWork_HandsOnExercise.Rmd", documentation=2)
```

